from rest_framework import serializers
from .models import Book, Author
from django.contrib.auth import get_user_model
from collections import OrderedDict


class CustomStringRelatedField(serializers.PrimaryKeyRelatedField):
    def to_representation(self, value):
        return str(value)
        
    def get_choices(self, cutoff=None):
        queryset = self.get_queryset()
        if queryset is None:
            return {}
        
        if cutoff is not None:
            queryset = queryset[:cutoff]
        
        return OrderedDict([
            (
                str(item.pk),
                self.display_value(item)
            )
            for item in queryset
        ])

class BookSerializer(serializers.ModelSerializer):
    authors = CustomStringRelatedField(queryset=Author.objects.all(), many=True, label="Autores")
    
    class Meta:
        model = Book
        fields = '__all__'
    
class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'
        

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    password_repeat = serializers.CharField(write_only=True, required=True)
    
    class Meta:
        model = get_user_model()
        fields = ('username', 'email', 'is_staff', 'pk', 'password', 'password_repeat', 'is_active')
        ref_name = 'User 1'
    
    def validate(self, attrs):
        if attrs['password'] != attrs['password_repeat']:
            raise serializers.ValidationError({"password": "Las contrasenas no coinciden"})
        return attrs
    
    def create(self, validated_data):
        User = get_user_model()
        password = validated_data.pop('password_repeat')
        u = User.objects.create(
            **validated_data
        )
        u.set_password(password)
        u.save()
        return u
    
    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.is_staff = validated_data.get('is_staff', instance.is_staff)
    
        if validated_data['password']:
            instance.set_password(validated_data['password'])
        instance.save()
        return instance
    
    def get_extra_kwargs(self):
        extra_kwargs = super().get_extra_kwargs()
        action = self.context['view'].action if 'view' in self.context else None
        
        if action in ['update', 'partial_update']:
            kwargs = extra_kwargs.get('password', {})
            kwargs['required'] = False
            extra_kwargs['password'] = kwargs
    
            kwargs = extra_kwargs.get('password_repeat', {})
            kwargs['required'] = False
            extra_kwargs['password_repeat'] = kwargs
    
        return extra_kwargs
