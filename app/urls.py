from django.urls import include, path
from rest_framework.routers import DefaultRouter
from app import views

router = DefaultRouter()

router.register(r'authors', views.AuthorViewSet)
router.register(r'books', views.BookViewSet)
router.register(r'users', views.UserViewSet)

urlpatterns = [
    path('', include(router.urls))
]