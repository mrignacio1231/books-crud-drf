from django.core.exceptions import ValidationError


def ISBNValidator(isbn):
    isbn_to_check = isbn.replace('-', '').replace(' ', '')
    
    if len(isbn_to_check) != 10 and len(isbn_to_check) != 13:
        raise ValidationError('Invalid ISBN: Longitud incorrecta')
    
    return True