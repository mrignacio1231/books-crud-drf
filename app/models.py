from django.db import models
from .validators import ISBNValidator

# Create your models here.

class Author(models.Model):
    first_name = models.CharField(verbose_name="Nombres", max_length=120)
    last_name = models.CharField(verbose_name="Apellidos", max_length=120)
    
    def __str__(self):
        return f'{self.first_name} {self.last_name}'
        
class Book(models.Model):
    isbn = models.CharField(verbose_name="ISBN", max_length=20, unique=True, blank=True, validators=[
        ISBNValidator
    ])
    name = models.CharField(verbose_name="nombre", max_length=200)
    pages = models.IntegerField(verbose_name="paginas", blank=True, default=0)
    publication_date = models.DateField(verbose_name="Fecha de publicacion", blank=True, null=True)
    authors = models.ManyToManyField(Author, verbose_name="Autores")
    
    def __str__(self):
        return self.name