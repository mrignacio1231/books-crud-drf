from django.shortcuts import render
from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from drf_renderer_xlsx.mixins import XLSXFileMixin
from .serializers import AuthorSerializer, BookSerializer, UserSerializer
from .models import Author, Book

# Create your views here.

class AuthorViewSet(XLSXFileMixin, viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Author.objects.all()
    model = Author
    serializer_class = AuthorSerializer
    filename = 'Autores.xlsx'
    
    
class BookViewSet(XLSXFileMixin, viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Book.objects.prefetch_related('authors').all()
    model = Book
    serializer_class = BookSerializer
    filename = 'Libros.xlsx'
    xlsx_custom_mappings = {
        'authors': lambda val: ', '.join(val)
    }
    xlsx_use_labels = True


class UserViewSet(XLSXFileMixin, viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
    filename = 'Usuarios.xlsx'