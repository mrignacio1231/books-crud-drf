# Books API
Este es un proyecto de ejemplo que usa Django y [Django REST Framework](https://github.com/encode/django-rest-framework/).

## Requisitos
Python 3.10

## Instalación
1. Clona este repositorio.
1. Instalar requerimientos. `pip install -r requirements.txt`
1. Ejecutar migraciones. `./manage.py migrate`
1. Crear usuario de pruebas. `./manage.py createsuperuser`
1. Iniciar servidor de pruebas. `./manage.py runserver`

## Documentación
La documentación completa del proyecto está disponible en [aqui](http://localhost:8000/swagger/).

## Login ejemplo

Hacer una petición POST en la ruta `/auth/jwt/create/`

Body:
```json
{
    "username": "example",
    "password": "secret"
}
```

## Consultar libros

Hacer una petición GET en la ruta `/api/books/`

Header:
```
Content-Type: application/json
Authorization: Bearer <TOKEN>
```

## Como exportar los datos a XLSX

`/api/books/?format=xlsx`